'use strict';
/*eslint no-underscore-dangle: ["error", { "allow": ["_doc", "_bar"] }]*/
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var ADMIN_ROLE = 'admin';
function getToken(user) {
    return jwt.sign(user, process.env.BACKOFFICE_SERVICE_API_SECRET_KEY);
}

var tokenTester = (token, callBack) => {
    var error = null;
    jwt.verify(token, process.env.BACKOFFICE_SERVICE_API_SECRET_KEY, {ignoreExpiration: true},
        function(err, decoded) {
            if (err) {
                //create a new error handler
                error = new Error('You are not authenticated');
                error.status = 401;
                return callBack(error);
            }
            if (decoded.role.indexOf(ADMIN_ROLE) === -1) {
                error = new Error('You are not authorized to perform this operation!');
                error.status = 401;
                return callBack(error);
            }
            return callBack();
        });
};

function verifyAdminUser(req, res, next) {
    if (req.swagger) {
        if ((req.swagger.params.body && req.swagger.params.body.path[1] === '/users/login') ||
            req.swagger.params.token) {
            return next();
        }
    }
    var error = null;
    // check header or url parameters or post parameters for token
    var token = req.headers.Authorization;
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, process.env.BACKOFFICE_SERVICE_API_SECRET_KEY, {ignoreExpiration: true},
            function(err, decoded) {
                if (err) {
                    //create a new error handler
                    error = new Error('You are not authenticated');
                    error.status = 401;
                    return next(error);
                }
                req.decoded = decoded;
                return next();
            });
    } else {
        //Not token provided status 401
        error = new Error('Not token provided');
        error.status = 401;
        return next(error);
    }
    return false;
}

module.exports = {
    getToken,
    verifyAdminUser,
    verifyIO: (token, callback) => {
        tokenTester(token, callback);
    }
};

