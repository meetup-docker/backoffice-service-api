'use strict';
var passport = require('passport');
var Verify = require('./../authenticate/verify');
var UserService = require('../services/user-service');

var authenticate = (req, res, next) => {
    passport.authenticate('local', function(errAuth, user, info) {
        if (errAuth) {
            return next(errAuth);
        }
        if (!user) {
            return res.status(401).json({
                err: info
            });
        }
        req.logIn(user, function(errLogIn) {
            if (errLogIn) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }
            var userDTO = {
                _id: user._id,
                username: user.username,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email
            };
            var token = Verify.getToken(userDTO);
            return res.status(200).json({
                status: 'Login successful!',
                user: userDTO,
                success: true,
                token: token
            });
        });
        return false;
    })(req, res, next);
};

var readUsers = (req, res, next) => {
    var searching = req.swagger.params.search.value || null;
    UserService.readUsers(searching).then((users) => {
        return res.json(users);
    }).fail((err) => {
        return next(err);
    });
};

var register = (req, res, next) => {
    UserService.createNewUser(req.body).then((user) => {
        return res.status(200).json({user: user});
    }).fail((err) => {
        return next(err);
    });
};

var deleteUser = (req, res, next) => {
    UserService.deleteUser(req.swagger.params.id.value).then((success) => {
        return res.status(204).json(success);
    }).fail((err) => {
        return next(err);
    });
};

var readUser = (req, res, next) => {
    UserService.readUserById(req.swagger.params.id.value).then((users) => {
        return res.json(users);
    }).fail((err) => {
        return next(err);
    });
};

module.exports = {
    authenticate, register, readUsers, deleteUser, readUser
};
