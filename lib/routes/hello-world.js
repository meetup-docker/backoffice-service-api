'use strict';

const helloWorld = require('../hello-world');

function hello(req, res) {
    const name = req.swagger.params.name.value || 'stranger';
    const text = helloWorld(name);
    res.json(text);
}

module.exports = {
    hello
};
