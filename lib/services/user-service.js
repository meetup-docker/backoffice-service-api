/*eslint id-length: ["error", {"min" : 1}]*/
/*eslint dot-notation: ["error", {"allowKeywords" : true}]*/
/*eslint-env es6*/
'use strict';
const QModule = require('q');
var User = require('../models/user');

var createNewUser = (userDto) => {
    var defer = QModule.defer();
    User.register(new User(userDto), userDto.password, function(errRegister, user) {
        if (errRegister) {
            var error = new Error(errRegister);
            error.username = userDto.username;
            error.status = 409;
            return defer.reject(error);
        }
        return defer.resolve(user);
    });
    return defer.promise;
};

var readUsers = (search) => {
    var defer = QModule.defer();
    var error = null;
    var query = {};
    if (search) {
        query.$or = [{
            username : new RegExp(search, 'i')
        }, {
            firstname : new RegExp(search, 'i')
        }, {
            lastname : new RegExp(search, 'i')
        }];
    }
    User.find(query, function(err, users) {
        if (err) {
            error = new Error(err);
            error.status = 409;
            return defer.reject(error);
        }
        return defer.resolve(users);
    });
    return defer.promise;
};

var updateUser = (userId, userDto) => {
    var defer = QModule.defer();
    var error = null;
    User.findByIdAndUpdate(userId, userDto, {new: true}, function(err, user) {
        if (err) {
            error = new Error(err);
            error.status = 409;
            return defer.reject(error);
        }
        return defer.resolve(user);
    });
    return defer.promise;
};

var deleteUser = (userId) => {
    var defer = QModule.defer();
    var error = null;
    User.findByIdAndRemove(userId, function(err) {
        if (err) {
            error = new Error(err);
            error.status = 409;
            return defer.reject(error);
        }
        return defer.resolve();
    });
    return defer.promise;
};

var readUserById = (userId) => {
    var defer = QModule.defer();
    var error = null;
    User.findById(userId, function(err, user) {
        if (err) {
            error = new Error(err);
            error.status = 409;
            return defer.reject(error);
        }
        return defer.resolve(user);
    });
    return defer.promise;
};

module.exports = {
    createNewUser,
    updateUser,
    readUsers,
    readUserById,
    deleteUser
};
