var should = require('should');
var HelloWorld = require('../../lib/hello-world');

describe('Hello World', function() {
  describe('Hello World return', function () {
    it('should return Hello World when execute HelloWorld function', function () {
      HelloWorld('Docker').should.be.equal('TEst says: Hello World!');
    });
  });
});
