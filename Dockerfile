FROM node:latest
MAINTAINER Rodrigo Perez
RUN apt-get update -y \
 && apt-get install -y git \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN npm config set registry http://registry.npmjs.org \
 && npm cache verify

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
RUN touch .env

COPY package.json /usr/src/app/
RUN npm install \
 && npm cache verify

COPY . /usr/src/app

CMD [ "npm", "start" ]
