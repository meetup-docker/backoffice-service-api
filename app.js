'use strict';

require('dotenv').load();
const express = require('express');
const app = express();
const cors = require('cors');
const compression = require('compression');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const logger = require('./lib/logger');
const expressWinston = require('express-winston');
const swaggerTools = require('swagger-tools');
const passport = require('passport');
var authenticate = require('./lib/authenticate/authenticate');
if (process.env.BACKOFFICE_SERVICE_API_IS_BEHIND_PROXY) {
    // http://expressjs.com/api.html#trust.proxy.options.table
    app.enable('trust proxy');
}

app.use(helmet.hidePoweredBy());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.frameguard());
app.use(helmet.xssFilter());
app.use(compression());
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// passport configuration
app.use(passport.initialize());
app.use(expressWinston.logger({
    winstonInstance: logger,
    expressFormat  : true,
    colorize       : true,
    meta           : false,
    statusLevels   : true
}));


//Baucis configuration
const mongoose = require('mongoose');
mongoose.connect('mongodb://' + process.env.BACKOFFICE_SERVICE_API_MONGODB_HOST + ':' + process.env.BACKOFFICE_SERVICE_API_MONGODB_PORT + '/' + process.env.BACKOFFICE_SERVICE_API_MONGODB_DB);
const buildBaucis = require('./build-baucis');
const baucisInstance = buildBaucis();


//Configure swagger-tools
const swaggerDoc = require('./swagger/swagger.json');
swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator());

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter({
        controllers          : './lib/routes',
        ignoreMissingHandlers: true,
        useStubs             : false // Conditionally turn on stubs (mock mode)
    }));

    app.use(expressWinston.errorLogger({
        winstonInstance: logger
    }));

    app.use(function(err, req, res, next) {
        if (res.headersSent) {
            return next(err);
        }
        res.status(err.status || 500);
        let error = {
            errorCode  : res.status,
            userMessage: err.message
        };

        if (process.env.NODE_ENV === 'development') {
            error.stack = err;
        }
        res.json(error);
    });

    app.use(function(req, res, next) {
        res.status(404).json({
            errorCode  : 404,
            userMessage: 'Not found.'
        });
    });

    // Start the server
    app.listen(process.env.BACKOFFICE_SERVICE_API_SERVER_PORT);
    logger.info('Your server is listening on port %d (http://%s:%d)', process.env.BACKOFFICE_SERVICE_API_SERVER_PORT, process.env.BACKOFFICE_SERVICE_API_SERVER_HOST,
        process.env.BACKOFFICE_SERVICE_API_SERVER_PORT);
});
